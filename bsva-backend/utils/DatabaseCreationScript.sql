-- Active: 1657781032510@@127.0.0.1@3306@BloodSugarVADB
-- CREATE USER 'bloodSugarVoiceAssistant'@'localhost' IDENTIFIED BY 'bsva-password';
-- GRANT ALL PRIVILEGES ON *.* TO 'bloodSugarVoiceAssistant'@'localhost' WITH GRANT OPTION;
-- FLUSH PRIVILEGES;

CREATE DATABASE BloodSugarVADB;
USE BloodSugarVADB;

CREATE TABLE users (
 id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 email VARCHAR(255) NOT NULL,
 password VARCHAR(255) NOT NULL
) DEFAULT CHARSET UTF8 COMMENT '';

CREATE TABLE glycemiaValues (
 id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
 value int NOT NULL, 
 createTime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 userID int NOT NULL ,
 CONSTRAINT fk_id  FOREIGN KEY (userId)  REFERENCES users(id)
 ON DELETE CASCADE
) DEFAULT CHARSET UTF8 COMMENT '';