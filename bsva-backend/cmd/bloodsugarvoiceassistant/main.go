// Blood Sugar Voice Assistant Backend Server
//
// Provides different handlers to proccess user and glucose value data
// Stores a random glucose value for every user every 10 minutes.
//
//Author: Edgar Diez Alonso, Universidad de Valladolid

package main

import (
	"log"
	"math/rand"
	"net/http"
	"time"
	"uva-bloodsugarvoiceassistant/pkg/controller"
	"uva-bloodsugarvoiceassistant/pkg/database"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/rs/cors"
)

// Launcher of Blood Sugar Voice Assistant Backend Server
func main() {
	//Connect to Database
	connectToDB()

	//Create Http Server
	log.Println("Starting the HTTP server on port 8090")
	router := mux.NewRouter().StrictSlash(true)

	//CORS policy
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"POST", "GET", "OPTIONS", "PUT", "DELETE"},
		AllowedHeaders: []string{"Accept", "content-type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
	})
	handler := c.Handler(router)

	//Create Handlers
	initHandlers(router)

	//Generate random blood glucose value every 10 minutes
	go func() {
		for true {
			var ids []int
			rows, err := database.Connector.DB().Query("SELECT DISTINCT id FROM users")
			if err == nil {
				var id int
				for rows.Next() {
					rows.Scan(&id)
					ids = append(ids, id)
				}

				for i := range ids {
					min := 40
					max := 350
					value := rand.Intn(max-min) + min
					database.Connector.Exec("INSERT INTO glycemiaValues (value, userID) VALUES (?, ?)", value, ids[i])
				}
				log.Println("Added new glucoses values!")
			}
			time.Sleep(1 * time.Minute)
		}
	}()
	log.Fatal(http.ListenAndServe(":8090", handler))
}

// Connects to a MySQL Database with the provided credentials
func connectToDB() {
	config :=
		database.Config{
			ServerName: "localhost:3306",
			User:       "bloodSugarVoiceAssistant",
			Password:   "bsva-password",
			DB:         "BloodSugarVADB",
		}
	connectionString := database.GetConnectionString(config)
	err := database.Connect(connectionString)
	if err != nil {
		panic(err.Error())
	}
}

// Handlers to proccess handlers for tables users and glycemiaValues
func initHandlers(router *mux.Router) {
	//User Handlers
	router.HandleFunc("/login", controller.Login).Methods("POST")
	router.HandleFunc("/user", controller.GetAllUsers).Methods("GET")
	router.HandleFunc("/user/{id}", controller.GetUserById).Methods("GET")
	router.HandleFunc("/user", controller.AddNewUser).Methods("POST")
	router.HandleFunc("/user/{id}", controller.DeleteUser).Methods("DELETE")

	//Glucose Handlers
	router.HandleFunc("/user/{id}/glucose/last", controller.GetLastGlucoseValueByUserId).Methods("GET")
	router.HandleFunc("/user/{id}/glucose/seven_days", controller.GetSevenDaysGlucoseValueByUserId).Methods("GET")
	///user/{id}/glucose?value={int}
	router.HandleFunc("/user/{id}/glucose", controller.AddGlucoseValueByUserId).Methods("POST")
	router.HandleFunc("/user/{id}/glucose", controller.DeleteGlucoseValuesByUserId).Methods("DELETE")

	//Alexa handler
	router.HandleFunc("/user/{email}/glucose/last_value", controller.GetLastGlucoseValueByUserEmail).Methods("GET")
}
