// Controllers for all the handlers defined in main.go

package controller

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"uva-bloodsugarvoiceassistant/pkg/database"
	"uva-bloodsugarvoiceassistant/pkg/entity"

	"github.com/gorilla/mux"
)

// Gets user by email and password
// Returns status not found if credentials do not match a user or user id if found
func Login(w http.ResponseWriter, r *http.Request) {
	requestBody, _ := ioutil.ReadAll(r.Body)
	var user entity.User
	json.Unmarshal(requestBody, &user)
	email := user.Email
	password := user.Password

	row := database.Connector.DB().QueryRow("SELECT * FROM users WHERE email = ? AND password = ?", email, password)
	err := row.Scan(&user.ID, &user.Email, &user.Password)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(user.ID)
	}
}

// Get all Users
func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	var users []entity.User
	database.Connector.Find(&users)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(users)
}

// Get User by ID.
// Returns status not found if user id doesn't exist
func GetUserById(w http.ResponseWriter, r *http.Request) {
	var user entity.User
	vars := mux.Vars(r)
	id := vars["id"]

	row := database.Connector.DB().QueryRow("SELECT * FROM users WHERE id = ?", id)
	err := row.Scan(&user.ID, &user.Email, &user.Password)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(user)
	}
}

// Add new User
// Returns Status Bad Request if the body doesn't contain all the required params (email and password)
// and Status Conflict when the email is already taken
func AddNewUser(w http.ResponseWriter, r *http.Request) {
	requestBody, _ := ioutil.ReadAll(r.Body)
	var user entity.User
	json.Unmarshal(requestBody, &user)
	if user.Email != "" && user.Password != "" {
		row := database.Connector.DB().QueryRow("SELECT * FROM users WHERE email = ?", user.Email)
		err := row.Scan(&user.ID, &user.Email, &user.Password)
		if err == nil {
			w.WriteHeader(http.StatusConflict)
		} else {
			database.Connector.Exec("INSERT INTO users (email, password) VALUES (?, ?)", user.Email, user.Password)
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusCreated)
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}

// Deletes User by id
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	database.Connector.Exec("DELETE FROM users WHERE id=?", id)
}

// Get Last Glucose Value for a user by userid
// Returns status not found if user id doesn't exist
func GetLastGlucoseValueByUserId(w http.ResponseWriter, r *http.Request) {
	var gvalue entity.GlycemiaValue
	vars := mux.Vars(r)
	id := vars["id"]
	row := database.Connector.DB().QueryRow("SELECT * FROM glycemiaValues WHERE userID= ? ORDER BY ID DESC LIMIT 1", id)
	err := row.Scan(&gvalue.ID, &gvalue.Value, &gvalue.CreateTime, &gvalue.UserId)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(gvalue)
	}
}

// Get last 7 days of glucose values for a user by user id
// Returns status not found if user id doesn't exist
func GetSevenDaysGlucoseValueByUserId(w http.ResponseWriter, r *http.Request) {
	var gvalues []entity.GlycemiaValue
	var gvalue entity.GlycemiaValue
	vars := mux.Vars(r)
	id := vars["id"]

	rows, err := database.Connector.DB().Query("SELECT * FROM glycemiaValues WHERE userID= ? AND (createTime >= DATE_SUB(CURDATE(), INTERVAL 7 DAY))", id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		for rows.Next() {
			rows.Scan(&gvalue.ID, &gvalue.Value, &gvalue.CreateTime, &gvalue.UserId)
			gvalues = append(gvalues, entity.GlycemiaValue{ID: gvalue.ID, Value: gvalue.Value, CreateTime: gvalue.CreateTime, UserId: gvalue.UserId})
		}
		if len(gvalues) == 0 {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(gvalues)
		}
	}
}

//Add Glucose Value for a user by user id
func AddGlucoseValueByUserId(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	//Check if user exists
	var user entity.User
	row := database.Connector.DB().QueryRow("SELECT * FROM users WHERE id = ?", id)
	err := row.Scan(&user.ID, &user.Email, &user.Password)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		value, err := strconv.Atoi(r.URL.Query().Get("value"))
		if err == nil && value > 0 {
			database.Connector.Exec("INSERT INTO glycemiaValues (value, userID) VALUES (?, ?)", value, id)
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusCreated)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
	}
}

// Delete Glucose Value for a user by user id
// Returns status not found if user id doesn't exist
func DeleteGlucoseValuesByUserId(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	//Check if user exists
	var user entity.User
	row := database.Connector.DB().QueryRow("SELECT * FROM users WHERE id = ?", id)
	err := row.Scan(&user.ID, &user.Email, &user.Password)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		database.Connector.Exec("DELETE FROM glycemiaValues WHERE userID=?", id)
		w.WriteHeader(http.StatusAccepted)
	}
}

// Get Last Glucose Value for a user by userid
// Returns status not found if user id doesn't exist
func GetLastGlucoseValueByUserEmail(w http.ResponseWriter, r *http.Request) {
	var gvalue entity.GlycemiaValue
	var id int 
	vars := mux.Vars(r)
	email := vars["email"]
	row := database.Connector.DB().QueryRow("SELECT id FROM users WHERE email= ?", email)
	err := row.Scan(&id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		row := database.Connector.DB().QueryRow("SELECT * FROM glycemiaValues WHERE userID= ? ORDER BY ID DESC LIMIT 1", id)
		err := row.Scan(&gvalue.ID, &gvalue.Value, &gvalue.CreateTime, &gvalue.UserId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(gvalue)
		}
	}
}
