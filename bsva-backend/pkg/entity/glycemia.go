package entity

import "time"

//Glycemia object for REST(CRUD)
type GlycemiaValue struct {
	ID        	int    		`json:"id"`
	Value 		int 		`json:"value"`
	CreateTime  time.Time   `json:"createTime"`
	UserId 		int			`json:"userId"`
}