package entity

//User object for REST(CRUD)
type User struct {
	ID        	int    		`json:"id"`
	Email 		string 		`json:"email"`
	Password 	string		`json:"password"`
}