# UVA-BloodSugarVoiceAssistant

Blood Sugar Voice Assistant is an Alexa skill that allows users to have a easy to use, in-depth control of their glycemia values over time.

The users of Blood Sugar Voice Assistant will be able to know their glycemia values in real time just by asking any Alexa compatible device with the skill installed, for example: **"Alexa, which is my glucose value?"**

Blood Sugar Voice Assistant also provides the user with **smart features**, such as:
   - Different recommendations based on the last measured glycemia value.
   - Intelligent alarms and notifications to remind the user to get another insuline shot or keep ingesting sugar if the glucose values haven't gone back to normal.
   - And so much more

## Architecture

Blood Sugar Voice Assistant is a 4 way application. First of all, the **Alexa skill** registers the actions that a user wants to take and forwards them to a **backend server**, which procceses them and registers the operation in a **MySQL Database**. In addition, users can also log in the **web application**, to create an account, check, download and/or delete their data.



### I- MySQL Database
Blood Sugar Voice Application's data will be stored in a MySQL Database, which will contain both **users** and **glycemia values**.

### II - Backend Server
All the petitions made by the user to Blood Sugar Voice Application will be processed by this server, programmed in Go (GoLang) as a **REST API**. The different petitions will by classified by different handlers which allow the connection to a MySQL Database, which stores user and glycemia data.

To fake glycemia values sent by a user's mobile phone, the server will upload random glucose values to the Database for every user registered every team minutes.

The server comprehends 4 different **packages**:
- Main: Launcher of the server.
- Controller: Manages the logic of the different handlers.
- Database: Manages the connection to the MySQL Database.
- Entity: Golang's representation of Database Tables.

### III - Web Application
The web application of Blood Sugar Voice Asssitant allows the user to register on the platform. The registration should be done with the same email that is registered in the Amazon account.
After login in the web, users can:
- See the recorded glycemia values of the last seven days.
- Export these values to a PDF.
- Logout.
- Delete all the glucose measurements stored in the Blood Sugar DataBase.
- Delete the user account completely.

### IV - Alexa Skill
Users should install the Blood Sugar Voice assisant from the Alexa Skill store and activate it with the voice command "Alexa, abre mi asistente de diabetes".
Then, to get their last measured blood glucose value they should say "Glucosa".
The skill will access the Data Base, retrieve the last stored value and depending on the level of glucose emit a recommendation. Also, if the glucose values are in the danger zones the skill will automatically program an alert to remind the user to check their glucose values to see if they are fixing them.
