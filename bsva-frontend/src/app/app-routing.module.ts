import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GlucoseListComponent } from './glucose-list/glucose-list.component';
import { UserLoginComponent} from './user-login/user-login.component';

const routes: Routes = [
  { path: 'glucose', component:GlucoseListComponent},
  { path: 'login', component:UserLoginComponent},
  { path: '**', redirectTo:'login', pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
