export interface Glycemia{
    id: Number,
    value: Number,
    createTime: Date,
    userID: Number
}