import { HttpClient,  HttpResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Glycemia } from './app.model';

@Injectable({
  providedIn: 'root'
})
export class ClientApiRestService {
  private static readonly BASE_URI = 'http://localhost:8090';

  constructor(private http: HttpClient) { }

  sendLogin(user: string): Observable<HttpResponse<any>> {
    let url = ClientApiRestService.BASE_URI + '/login';
    return this.http.post(url, user, {
      observe: 'response',
      responseType: 'text',
    });
  }
  sendRegister(user: string): Observable<HttpResponse<any>> {
    let url = ClientApiRestService.BASE_URI + '/user';
    return this.http.post(url, user, {
      observe: 'response',
      responseType: 'text',
    });
  }
  getUserGlucoseData(): Observable<HttpResponse<Glycemia[]>> {
    let id = localStorage.getItem('id');
    let url = ClientApiRestService.BASE_URI + '/user/' + id + '/glucose/seven_days';
    return this.http.get<Glycemia[]>(url, {observe: 'response'});
  }

  deleteUserGlucoseData(): Observable<HttpResponse<any>> {
    let id = localStorage.getItem('id');
    let url = ClientApiRestService.BASE_URI + '/user/' + id + '/glucose';
    return this.http.delete(url, {observe: 'response'});
  }

  deleteUserAccount(): Observable<HttpResponse<any>> {
    let id = localStorage.getItem('id');
    let url = ClientApiRestService.BASE_URI + '/user/' + id;
    return this.http.delete(url, {observe: 'response'});
  }

}
