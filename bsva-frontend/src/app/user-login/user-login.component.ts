import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientApiRestService } from '../shared/client-api-rest.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  constructor(
    private router: Router,
    private clientApiRest: ClientApiRestService,
  ) { }

  ngOnInit(): void {}

  // logs user in and saves user id and user email in local storage for session persistance
  login(email: string, password: string) {
    if(this.validateEmail(email) && password != ''){
      let jsonString =
        '{"email":"' + email + '", "password": "' + password + '"}';
      let json = JSON.parse(jsonString);
      this.clientApiRest.sendLogin(json).subscribe(
        (resp) => {
          if (resp.status < 400) {
            localStorage.setItem('id', resp.body);
            localStorage.setItem('email', email);
            if (
              localStorage.getItem('id') != null &&
              localStorage.getItem('id') != ''
            )
              this.router.navigate(['glucose']);
          }
        },
        (err) => {
          alert("No user found with the provided credentials")
          console.log('Login error: ' + err.message);
          throw err;
        }
      );
    }
    // if it doesnt match email
    else{
      alert("Please introduce email and password");
      (<HTMLFormElement>document.getElementById("loginForm")).reset();
    }
  }

  // register a new user in the application
  register(email: string, password: string) {
    if(this.validateEmail(email) && password != ''){
      let jsonString =
        '{"email":"' + email + '", "password": "' + password + '"}';
      let json = JSON.parse(jsonString);
      this.clientApiRest.sendRegister(json).subscribe(
        (resp) => {
          if (resp.status < 400) {
            alert("Your user has been created");
            (<HTMLFormElement>document.getElementById("loginForm")).reset();
          }
        },
        (err) => {
          if (err.status == 409){
            alert("User already registered!");
            (<HTMLFormElement>document.getElementById("loginForm")).reset();
          }
          else {
            alert("Error trying to create your user")
            console.log('Login error: ' + err.message);
            throw err;
          }
        }
      );
  }
  // if it doesnt match email
  else{
    alert("Please introduce email and password");
    (<HTMLFormElement>document.getElementById("loginForm")).reset();
  }
  }

  // validates email format, returns true if the string matches an email
  validateEmail(email:string){
    let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return regexp.test(email);
  }
}
