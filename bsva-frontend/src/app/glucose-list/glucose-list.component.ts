import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Glycemia } from '../shared/app.model';
import { ClientApiRestService } from '../shared/client-api-rest.service';

// Print pdf modules
import {jsPDF} from 'jspdf'; 

@Component({
  selector: 'app-glucose-list',
  templateUrl: './glucose-list.component.html',
  styleUrls: ['./glucose-list.component.css']
})
export class GlucoseListComponent implements OnInit {

  GlucoseValues: Glycemia[] = [];
  @ViewChild('pdfTable') pdfTable:ElementRef;  

  constructor(
    private clientApiRest: ClientApiRestService,
    private router: Router
  ) { }

  ngOnInit(): void {
    let email = localStorage.getItem('email');
    let id = localStorage.getItem('id');
    if(email == '' || id == ''){
      this.logout()
    }
    document.getElementById('logout')!.textContent = 'Logout: ' + email;
    this.getUserGlucoseData()
  }

  getUserGlucoseData() {
    this.clientApiRest.getUserGlucoseData().subscribe(
      (resp) => {
        if (resp.status < 400) {
          this.GlucoseValues = resp.body as Glycemia[];
        }
      },
      (err) => {
        console.log('Error loading user glucose data' + err.message);
        throw err;
      }
    );
  }
  logout() {
    localStorage.setItem('id', '');
    localStorage.setItem('email', '');
    this.router.navigate(['login']);
  }

  deleteGlucoseForUser(){
    let deleteData = window.confirm("Are you sure you want to delete all the glucose records?");
    if (deleteData){
      this.clientApiRest.deleteUserGlucoseData().subscribe(
        (resp) => {
          if (resp.status < 400) {
              alert("All the glucose records have been deleted.");
              this.logout();
          }
        },
        (err) => {
          console.log('Error deleting user glucose data' + err.message);
          throw err;
        }
      );
    }
  }

  deleteUserAccount(){
    let deleteData = window.confirm("Are you sure you want to delete your account and all the data associated to it?");
    if (deleteData){
      this.clientApiRest.deleteUserAccount().subscribe(
        (resp) => {
          if (resp.status < 400) {
              alert("Your account and all the glucose records have been deleted.");
              this.logout();
          }
        },
        (err) => {
          console.log('Error deleting user glucose data' + err.message);
          throw err;
        }
      );
    }
  }

  printPDF() {
    window.print();
  }
}
