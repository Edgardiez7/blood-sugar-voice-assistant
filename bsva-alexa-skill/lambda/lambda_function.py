# -*- coding: utf-8 -*-

# This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK for Python.
# Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
# session persistence, api calls, and more.
# This sample is built using the handler classes approach in skill builder.
import logging
import ask_sdk_core.utils as ask_utils
import requests

from ask_sdk_core.skill_builder import CustomSkillBuilder
from ask_sdk_core.api_client import DefaultApiClient
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler
from ask_sdk_core.handler_input import HandlerInput

from ask_sdk_model import Response
from ask_sdk_model.ui import AskForPermissionsConsentCard

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

REQUIRED_PERMISSION_TIMER = ["alexa::alerts:timers:skill:readwrite"]
REQUIRED_PERMISSION_EMAIL = ["alexa::profile:email:read"]

def get_custom_task_launch_timer(duration):
    return {
        "duration": f"PT{duration}M",
        "timerLabel": "BloodSugarTimer",
        "creationBehavior": {
        "displayExperience": {
                "visibility": "VISIBLE"
            }
        },
        "triggeringBehavior": {
            "operation": {
                "type": "ANNOUNCE",
                "textToConfirm": [{
                    "locale": "es-ES",
                    "text": "El temporizador de Blood Sugar Voice Assistant ha finalizado. Por favor, mida de nuevo su nivel de azúcar."
                }],
            },
            "notificationConfig": {
                "playAudible": False
            }
        }
    }

class BloodSugarVoiceAssistantController:

    def __init__(self):
        self.user_email = ""
        self.glucose_vals = []
        self.alertHypoglucemic = False
        self.alertHyperglucemic = False
        self.speak_output = ""
        
    def setUserEmail(self, email):
        self.user_email = email

    def getLastGlucoseValueForUser(self):
        url = f"http://157.88.125.240:20212/user/{self.user_email}/glucose/last_value"
        response = requests.get(url)
        if response.status_code == 200:
            glucose_val = response.json()["value"]
            self.glucose_vals.append(glucose_val)
        else:
            self.glucose_vals.append(None)

    def setSpeechOutputForLastGlucoseValue(self):
        if self.glucose_vals:
            glucose_val = self.glucose_vals[-1]
            if glucose_val <= 70:
                self.speak_output = f"Su último valor de glucosa registrado es {glucose_val}. Tiene hipoglucemia. Por favor, ingiera hidratos de carbono de absorción rápida como zumos, refrescos, caramelos con azúcar, o similares."
            elif 70 < glucose_val < 120:
                self.speak_output = f"Su último valor de glucosa registrado es {glucose_val}. Su valor de glucosa es ideal"
            elif 120 <= glucose_val < 180:
                self.speak_output = f"Su último valor de glucosa registrado es {glucose_val}. Su valor de glucosa es un poco alto. Tenga cuidado!"
            elif 180 <= glucose_val < 250:
                self.speak_output = f"Su último valor de glucosa registrado es {glucose_val}. Tiene hiperglucemia. Por favor, realice ejercicio físico para corregir su nivel de azúcar"
            else:
                # glucosa >= 250
                self.speak_output = f"Su último valor de glucosa registrado es {glucose_val}. Tiene hiperglucemia. Por favor, corrija su valor de glucemia con insulina urgentemente."
            
    def processAlerts(self, handler_input):
        timer = 0
        if self.glucose_vals:
            glucose_val = self.glucose_vals[-1]
            if len(self.glucose_vals) > 1:
                previous_glucose_val = self.glucose_vals[-2]
            
            # If it is the first time
            if not self.alertHypoglucemic and not self.alertHyperglucemic:
                if glucose_val < 70:
                    self.alertHypoglucemic = True
                    # lanzar intent en 15 mins
                    timer = 15
                elif glucose_val > 180:
                    self.alertHyperglucemic = True
                    # lanzar intent en 30 mins
                    timer = 30
                    
            # If there is a hypoglucemic alert programmed        
            elif self.alertHypoglucemic:
                if 70 < glucose_val < 180:
                    self.speak_output = self.speak_output.split(".")[0] + " . Ha corregido su hipoglucemia. Le recomendamos ingerir hidratos de carbono de absorción lenta. Buen trabajo!"
                    self.alertHypoglucemic = False
                elif glucose_val >= 180:
                    self.speak_output = self.speak_output.split(".")[0] + " . Ha corregido su hipoglucemia. Sin embargo ahora tiene hiperglucemia. Por favor, realice ejercicio físico para corregir su valor de glucemia."
                    self.alertHypoglucemic = False
                    self.alertHyperglucemic = True
                    # lanzar intent en 30 mins
                    timer = 30
                elif glucose_val <= 70 and glucose_val > previous_glucose_val:
                    self.speak_output = self.speak_output.split(".")[0] + " . Sigue con hipoglucemia pero está corrigiendo sus valores de azúcar en sangre. Le recomendamos ingerir hidratos de carbono de absorción lenta."
                    # lanzar intent en 15 mins
                    timer = 15
                else: # glucose < 70 and lower than previous glucose value
                    self.speak_output = self.speak_output.split(".")[0] + " . Sigue con hipoglucemia y sus valores han empeorado. Por favor, ingiera hidratos de carbono de absorción rápida como zumos, refrescos, caramelos con azúcar, o similares."
                    # lanzar intent en 15 mins
                    timer = 15
                    
            # If there is a hyperglucemic alert programmed        
            elif self.alertHyperglucemic:
                if 70 < glucose_val < 180:
                    self.speak_output = self.speak_output.split(".")[0] + " . Ha corregido su hiperglucemia. Buen trabajo!"
                    self.alertHyperglucemic = False
                elif glucose_val <= 70:
                    self.speak_output = self.speak_output.split(".")[0] + " . Ha corregido su hiperglucemia. Sin embargo ahora tiene hipoglucemia. Le recomendamos ingerir hidratos de carbono de absorción rápida como zumos, refrescos, caramelos con azúcar, o similares."
                    self.alertHyperglucemic = False
                    self.alertHypoglucemic = True
                    # lanzar intent en 15 mins
                    timer = 15
                elif glucose_val >= 180 and glucose_val < previous_glucose_val:
                    self.speak_output = self.speak_output.split(".")[0] + " . Sigue con hiperglucemia pero está corrigiendo sus valores de azúcar en sangre. Por favor, realice ejercicio físico para corregir su nivel de azúcar."
                    # lanzar intent en 1 hora
                    timer = 60
                else: # glucose >= 180 and higher than previous glucose value
                    self.speak_output = self.speak_output.split(".")[0] + " . Sigue con hiperglucemia y sus valores han empeorado. Por favor, realice ejercicio físico y corrija su valor de glucemia con insulina urgentemente."
                    # lanzar intent en 30 mins
                    timer = 30
        return timer

class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        # Set the user email for the amazon account (has to be the same than BSVA to work)
        try:
            ups_service_client = handler_input.service_client_factory.get_ups_service()
            profile_email = ups_service_client.get_profile_email()
            if profile_email is None:
                return handler_input.response_builder.speak("Su cuenta de Amazon no tiene un email vinculado. Por favor, vincule el email registrado en Blood Sugar Voice Assistant.").response
            else:
                bsvaController.setUserEmail(profile_email)
        except:
            return (
                    handler_input.response_builder
                        .speak("Por favor, conceda permisos para obtener su email usando la aplicación de Alexa.")
                        .set_card(AskForPermissionsConsentCard(permissions=REQUIRED_PERMISSION_EMAIL))
                        .response
                    )
        speak_output = f"Bienvenido a Blood Sugar Voice Assistant. Puede solicitar su último valor de glucosa registrado diciendo: glucosa o azúcar."
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask("Puede solicitar de nuevo su valor de glucosa diciendo: glucosa o azúcar.")
                .response
        )


class LastGlucoseValueHandler(AbstractRequestHandler):
    """Handler for LastGlucoseValue Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("LastGlucoseValue")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        bsvaController.getLastGlucoseValueForUser()
        if bsvaController.glucose_vals[-1] != None:
            bsvaController.setSpeechOutputForLastGlucoseValue()
            try:
                timer_duration = bsvaController.processAlerts(handler_input)
                timer_duration_str = ''
                if timer_duration > 0:
                    timer_dict = get_custom_task_launch_timer(timer_duration)
                    accessToken = ask_utils.get_api_access_token(handler_input)
                    options = {"Authorization": "Bearer " + accessToken, "Content-Type": "application/json"}
                    response = requests.post("https://api.amazonalexa.com/v1/alerts/timers", timer_dict, options)
                    timer_duration_str = " Se ha establecido un temporizador de " + str(timer_duration) + " minutos. Por favor, compruebe de nuevo su nivel de azúcar en sangre al finalizar el temporizador."
            except:
                return (
                        handler_input.response_builder
                            .speak("Por favor, conceda permisos para establecer temporizadores usando la aplicación de Alexa.")
                            .set_card(AskForPermissionsConsentCard(permissions=REQUIRED_PERMISSION_TIMER))
                            .response
                        )
            return (
                handler_input.response_builder
                    .speak(bsvaController.speak_output + timer_duration_str)
                    #.ask("Puede solicitar de nuevo su valor de glucosa diciendo: glucosa o azúcar.")
                    .response
            )
        else:
            return (
                handler_input.response_builder
                    .speak("El email de su cuenta de Amazon no está registrado en la aplicación Blood Sugar Voice Assistant. Por favor, acceda a la web de la aplicación para crearse una cuenta de usuario con el mismo correo electrónico que su cuenta de Amazon")
                    #.ask("Puede solicitar de nuevo su valor de glucosa diciendo: glucosa o azúcar.")
                    .response
            )


class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Puede solicitar su último valor de glucosa registrado diciendo: glucosa o azúcar."

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return (ask_utils.is_intent_name("AMAZON.CancelIntent")(handler_input) or
                ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input))

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Hasta luego! Gracias por usar Blood Sugar Voice Assistant."

        return (
            handler_input.response_builder
                .speak(speak_output)
                .response
        )

class FallbackIntentHandler(AbstractRequestHandler):
    """Single handler for Fallback Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.FallbackIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In FallbackIntentHandler")
        speech = "Puede solicitar su último valor de glucosa registrado diciendo: glucosa o azúcar."
        reprompt = "Puede solicitar su último valor de glucosa registrado diciendo: glucosa o azúcar."

        return handler_input.response_builder.speak(speech).ask(reprompt).response

class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response

        # Any cleanup logic goes here.

        return handler_input.response_builder.response


class IntentReflectorHandler(AbstractRequestHandler):
    """The intent reflector is used for interaction model testing and debugging.
    It will simply repeat the intent the user said. You can create custom handlers
    for your intents by defining them above, then also adding them to the request
    handler chain below.
    """
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("IntentRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        intent_name = ask_utils.get_intent_name(handler_input)
        speak_output = "You just triggered " + intent_name + "."

        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors. If you receive an error
    stating the request handler chain is not found, you have not implemented a handler for
    the intent being invoked or included it in the skill builder below.
    """
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        speak_output = "Ha surjido un problema al procesar su petición. Puede solicitar su último valor de glucosa registrado diciendo: glucosa o azúcar."

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )

# The SkillBuilder object acts as the entry point for your skill, routing all request and response
# payloads to the handlers above. Make sure any new handlers or interceptors you've
# defined are included below. The order matters - they're processed top to bottom.


sb = CustomSkillBuilder(api_client=DefaultApiClient())
# Create controller for the Blood Sugar Voice Assistant Skill
bsvaController = BloodSugarVoiceAssistantController()

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(LastGlucoseValueHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(FallbackIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_request_handler(IntentReflectorHandler()) # make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers

sb.add_exception_handler(CatchAllExceptionHandler())

lambda_handler = sb.lambda_handler()